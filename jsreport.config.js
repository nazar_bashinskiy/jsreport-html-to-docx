module.exports = {
    name: 'html-to-docx',
    dependencies: ['templates'],
    main: 'lib/index.js',
    embeddedSupport: true
}