# jsreport-html-to-docx
It is custom receipe for [jsreport-core](https://github.com/jsreport/jsreport-core "jsreport-core") to genereate docx from html template.
Based on [html-to-docx](https://github.com/Aymkdn/html-to-pdfmake)
## Usage 


    const jsreport = require('jsreport-core')();
    const handlebars = require('jsreport-handlebars');
    const htmlDocxReceipe = require('jsreport-html-to-docx');
    
    const fs = require('fs')
    
    async function render(text){
        await jsreport.init();
        jsreport.use(handlebars);
        jsreport.use(htmlDocxReceipe)
    
        const pdf = await jsreport.render({
            template: {
                content: text,
                engine: 'handlebars',
                recipe: 'html-to-docx',
            },
            data: {
                test: 'test2'
            }
        });
    
        return pdf
    }
    
    render('<div><h1><span data-type="highlight" style="background-color: #ffff00;"><strong>{{test}}</strong></span></h1></div>')
        .then(res => fs.writeFileSync('./temp.docx', res.content))