const htmlDocx = require('./html-to-docx')

module.exports = reporter => {
    reporter.documentStore.model.entityTypes['TemplateType'].contentType = { type: 'Edm.String' }
    reporter.documentStore.model.entityTypes['TemplateType'].fileExtension = { type: 'Edm.String' }
    reporter.documentStore.model.entityTypes['TemplateType'].contentDisposition = { type: 'Edm.String' }

    reporter.extensionsManager.recipes.push({
        name: 'html-to-docx',
        execute: async (request, response) => {
            response.meta.contentType = request.template.contentType || 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            response.meta.fileExtension = request.template.fileExtension || '.docx'
            response.meta.contentDisposition = `attachment; filename="report.docx"`

            response.content = await htmlDocx(response.content.toString())
        }
    })
}